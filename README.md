# CI/CD Pipeline with Jenkins, GitLab, and Terraform

By Ernest Awangya

In this article, we will be providing a comprehensive guide for creating and deploying your CI/CD pipeline with Jenkins, GitLab and Terraform.

This repository contains a Jenkins Pipeline script that automates the Continuous Integration and Continuous Deployment (CI/CD) process for deploying a Dockerized Node.js application onto an EC2 instance. The pipeline involves building the Docker image, provisioning an EC2 server using Terraform, and deploying the application to the EC2 instance.

## Prerequisites

Before setting up the pipeline, make sure you have the following prerequisites in place:
## Install the following in the jenkins container / server:
(Recomendated)
- Terraform
- AWSCLi

(Per application you are deploy)
For node:
- nodejs
- npm


1. Jenkins:

## Installation

   To configure automated CI/CD with Jenkins, the first and the most obvious step is to install Jenkins. Jenkins setup, with complete instructions on how to install it, is available here, Here, We will use Jenkins container to install Jenkina

 ```
 #!/bin/bash

sudo yum update -y \
&& sudo yum install docker -y \
&& sudo usermod -aG docker ec2-user \
&& sudo systemctl enable docker \
&& sudo systemctl start docker

docker run --name jenkins -d -p 8080:8080 -p 50000:50000 -d -v jenkins_home:/var/jenkins_home -v /var/run/docker.sock:/var/run/docker.sock -v $(which docker):/usr/bin/docker jenkins/jenkins:lts

 ```  

## Starting the Server
## Post-Installation Setup

When you configure Jenkins for the first time, it is crucial to unlock it using an automatic password. For this setup, type http://localhost:8080 in the browser, and wait for the Getting Started page to appear. The automatically-generated password is stored in /var/jenkins_home/secrets/initialAdminPassword file. The file can be also accessed via the command-line interface using the cat command with the filename. 

Copy/paste the password in the Administrator Password field and click Continue. The dialogue box will close and that’s it! You have successfully set up the user. After that, you will get the Customize Jenkins page. From this page, you can install any number of plugins you require. You can install suggested plugins as they are selected by default and you can also choose any additional plugins. Make sure to install the Git, Pipeline: AWS Steps and SSH AgentPlugin.

## Connecting Jenkins Server with GitLab
Add credents that will enable Jenkins Server to connect with GitLab.On the Jenkins dashboard, click on “Manage Jenkins”. Add Credentials:
On the "Credentials" page, you will see a list of credential domains (system, global, and other domains if configured). Choose the appropriate domain for your credentials or create a new one.

Add Credentials Item:Click on "Add Credentials" or "global" (if you're using the global domain) to add a new credential.

Select Credential Type: You'll see a dropdown with various credential types. Choose the appropriate type based on your use case:

`Username and Password:` For storing a username and password combination.

`Secret Text:` For storing sensitive text, such as API tokens or secrets.

`SSH Username with Private Key:` For storing an SSH private key along with a username.

Fill in Details:
Depending on the credential type, you'll need to provide different details:

For `"Username and Password,"` enter the username and password.

For `"Secret Text,"` enter the secret value and provide an optional description.

For `"SSH Username with Private Key,"` enter the username, upload the private key, and provide an optional passphrase if the key is encrypted.

Save:

After filling in the required details, click on "OK" or "Save" to create the credential.

Verify:
Once saved, you should see the newly created credential listed in the appropriate credential domain.

We will create credentials for:
- Gitlab, Type: `Username and Password`
- Docker Hub, Type: `Username and Password`
- Jenkins aws_access_key_id, Type: `Secret Text`
- Jenkins aws_secret_acces_key, Type: `Secret Text`
- Jenkins server, ! container SSH Key, Type: `SSH Username with Private Key` 
- Jenkins Credentials to use with AWS CLI, Type: `Username(aws_access_key_id) and Password(aws_secret_acces_key)`, For the AWS CLI credentials, the username field with take in Jenkins aws_access_key_id and the password field will take in the aws_secret_acces_key.

<details>
<summary>Install the following inside the Jenkins Container</summary>

- [Terraform](https://developer.hashicorp.com/terraform/tutorials/aws-get-started/install-cli)
- [AWS CLI](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html)
- [NODEJS ](https://nodejs.org/en/download)
- [NPM ](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm)

</details>

Use in Pipelines:
In your Jenkins pipeline scripts (Jenkinsfiles), you can use the withCredentials step to securely access the stored credentials during your build or deployment steps.



