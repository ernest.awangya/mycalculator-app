resource "aws_instance" "my-app" {
  ami           = data.aws_ami.my-lastest-ami.id
  instance_type = "t2.micro"
  subnet_id = aws_subnet.my_subnet.id
  vpc_security_group_ids = [ aws_security_group.my_sg.id ]
  associate_public_ip_address = true
  key_name = aws_key_pair.my_keypair.key_name

  user_data = file("linux-server.sh")
 
  # connection {
  #   type = "ssh"
  #   host = self.public_ip
  #   user = "ubuntu"
  #   private_key = "~/ssh/id"
  # }
  
  # provisioner "file" {
  #   source = "index.html"
  #   destination = "/home/ubuntu/index.html"
  # }

  # provisioner "remote-exec" {
  ##   script = file("entry-script.sh")

  # }

  tags = {
    Name = "linux-server"
  }
}