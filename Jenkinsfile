pipeline {
    agent any

    environment {
    // Define the image version based on the Jenkins build number
    IMAGE_VERSION = "${BUILD_NUMBER}"
    }

    stages {
        // stage('GitHub Checkout') {
        //     steps {
        //         git branch: 'main', credentialsId: 'gitlab-creds', url: 'https://gitlab.com/ernest.awangya/mycalculator-app.git'
        //     }
        // }
        
        // run in jenkins container chmod 666 /var/run/docker.sock
        stage('Docker Build and Push') {
            steps {
                withCredentials([usernamePassword(credentialsId: 'docker_creds', passwordVariable: 'PASS', usernameVariable: 'USER')]) {
                    sh 'echo $PASS | docker login -u $USER --password-stdin'
                    sh 'docker build -t eawangya/mycalculator-app:calculator-${BUILD_NUMBER} .'
                    sh 'docker push eawangya/mycalculator-app:calculator-${BUILD_NUMBER}'
                    
                }
            }
        }
        
        stage('Provision EC2 Server') {
            environment {
                AWS_ACCESS_KEY_ID = credentials('jenkins_aws_access_key_id')
                AWS_SECRET_ACCESS_KEY = credentials('jenkins_aws_secret_access_key')
            }

            // cat ~/.ssh/id_rsa.pub of jenkins container and edit keypair.tf file
            steps {
                script {
                    dir('terraform') {
                        sh 'terraform init'
                        //sh 'terraform apply --auto-approve'
                        sh 'terraform destroy --auto-approve'
                        EC2_LINUX_IP = sh(
                            script: "terraform output ec2_public_ip",
                            returnStdout: true
                        ).trim()

                        INSTANCE_ID = sh(
                            script: "terraform output ec2_instance_id", // Update this with the correct output name
                            returnStdout: true
                        ).trim()

                        echo "EC2 Linux Instance IP: $EC2_LINUX_IP"
                        echo "EC2 Instance ID: $INSTANCE_ID"
                    }
                }
            }
        }
        
        stage('Wait for EC2 Instance to be Running') {
    steps {
        script {
            withAWS(region: 'us-east-1', credentials: 'awscli-creds') {
                def instanceStatus = ''
                def systemStatus = ''
                def maxRetries = 50
                def retryCount = 0

                while ((instanceStatus != 'running' || systemStatus != 'ok') && retryCount < maxRetries) {
                    sleep 5 // Wait for 30 seconds before checking again

                    def instanceStatusOutput = sh(
                        script: "aws ec2 describe-instance-status --instance-ids $INSTANCE_ID --query 'InstanceStatuses[0].InstanceState.Name' --output text",
                        returnStdout: true
                    ).trim()
                    instanceStatus = instanceStatusOutput ?: 'pending'

                    def systemStatusOutput = sh(
                        script: "aws ec2 describe-instance-status --instance-ids $INSTANCE_ID --query 'InstanceStatuses[0].SystemStatus.Status' --output text",
                        returnStdout: true
                    ).trim()
                    systemStatus = systemStatusOutput ?: 'insufficient-data'

                    echo "Instance status: $instanceStatus"
                    echo "System status: $systemStatus"

                    if (instanceStatus != 'running' || systemStatus != 'ok') {
                        retryCount++
                        echo "Retrying in 30 seconds..."
                    }
                }

                if (instanceStatus == 'running' && systemStatus == 'ok') {
                    echo "Instance and system status are satisfactory. Proceed with deployment."
                } else {
                    error "Instance or system status did not pass the checks within the specified retries."
                }
            }
        }
    }
}

        stage('Deploy to EC2 Instance') {
            steps {
                script {
                    echo 'Waiting for server to initialize'
                    // sleep(time: 90, unit: "SECONDS")
                    sshagent(['jenkins_server_key']) { // Install SSH Agent plugin, key name should be that of Jenkins
                       sh """
                            ssh -o StrictHostKeyChecking=no ec2-user@$EC2_LINUX_IP '
                            docker stop mycalculator-container || true
                            docker rm mycalculator-container || true
                            docker run -d -p 3000:3000 --name mycalculator-container eawangya/mycalculator-app:calculator-${BUILD_NUMBER}'
                          """
                    }
                }
            }
        }
    }
}
