FROM node:20

WORKDIR /usr/app

COPY package*.json ./

RUN npm install

COPY app.js index.html ./

CMD [ "node", "app.js" ]
